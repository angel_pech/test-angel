1. ¿Has aplicado los principios SOLID?
No al 100%, ya que apenas estoy aprendiendo sobre los principios SOLID, pero trato de distribuir las cargas que
se presentan en el proyecto, como clases (archivos dedicados) para realizar peticiones y modular el store de VUEX
para distribuir y bien la estructura y sea fácil de entender y sea mantenible.

2. ¿Cuánto tiempo has estado pensando y escribiendo tests del código? Si hubieras tenido mucho más tiempo... ¿qué habrías añadido? Si no has realizado tests, ¿qué herramientas habrías utilizado para llevarlos a cabo?
R1. No pude realizar test.
R2. Hubiera mejorado un poco más el diseño. Un mejor control en la paginación para cuando se recargue la página no se pierda la query y siga mostrando el contenido antes del refres. Hubiera añadido más filtros y detalles pero traté de cubrir lo esencial como prueba de que pude aplicarlo.
R3. Utilizaría las herramientas 'Mocha y Chai' he leido y visto, en vídeos, que son buenas herramientras entre muchas otras. Pero al ser una de las que por defecto viene en 'Vue Cli', recomiendan usarlo.

3. ¿Cómo mejorarías la API que has usado?
Añadiría una propiedad en el RESPONSE que señale el total de los registros según el filtro. Sin contar el 'per_page' o 'page'.

4. ¿Crees que esta API soporta peticiones CORS? ¿Cómo has llegado a esa conclusión?
Sí soporta peticiones, pero no es necesario configurar el cliente HTTP, en mi caso Axios. Ya que tiene acceso público.

5. ¿Cómo rastrearías un problema de rendimiento en producción? ¿Alguna vez has tenido que hacerlo?
    - Comentarios de los usuario
    - Horarios donde se generen los bajos rendimiendo o cuellos de botella
    - Logs de errores
    - Picos en los graficos de los comonentes del servidor (si hay registros con horario, mejor)
    - Pruebas de extrés

    No he tenido la oportunidad pero he visto que se realizar ciertas pruebas.

6. Descríbete a ti mismo en formato JSON

{
    "nombres": "Angel Gabriel",
    "primer_apellido": "Pech",
    "segundo_apellido": "Rosales",
    "titulo": "Ingeniero en Sistemas Computacionales",
    "sexo": "h",
    "numero_telefono": "9831849368",
    "fecha_nacimiento": "15/10/1996",
    "estatura": {
        "valor": 1.6,
        "unidad": metros,
    },
    "cabello": {
        "tipo": "risado",
        "color": "negro",
    },
    "comidas_favoritas": [
        "Ceviche",
        "Caldo de pollo",
    ],
    "deportes_favoritos": [
        "Voleibol",
        "Beisbol
    ],
}

Axios
MDI - Material Design Icons

La estrctura la hago por componentes, ayuda a tener una mejor administración del proyecto y hace
tener la facilidad de entender el código ya que no está todo en un solo archivo.

Uso Typescript para tener un código limpio y legible, al igual que usar eslint.

El index es donde se encuentra la lista de registros.
Dentro de la lista de ubican los cards que muestran la información más relevante del registro.
Al seleccionar el card se despliega un dialog que permite visualizar la información extra del registro.
En la vista principal existe un filtro para buscar u obtener los registros bajo esos parametros.
La paginación permite avanzar por cantidad y páginas de registros.
Uso un archivo dedicado para hacer peticiones el cual es accesible importando dicho archivo.
Se crearon componentes de varios elementos para reducir el código y tener una mejor legibilidad del código.
