import Vue from 'vue';
import Vuex, { StoreOptions } from 'vuex';

import { Root } from '@/store/types';
import Beers from '@/store/Beers';

Vue.use(Vuex);

const root: StoreOptions<Root> = {
	state: {
		module_name: '',
	},

	mutations: {
		SET_MODULE_NAME(state, payload) {
			state.module_name = payload;
		},
	},

	actions: {
		SET_MODULE_NAME({ commit }, payload) {
			commit('SET_MODULE_NAME', payload);
		},
	},

	modules: {
		Beers,
	},
};

export default new Vuex.Store<Root>(root);
