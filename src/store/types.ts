export interface Root {
    module_name: string;
}

export interface Malt {
    name: string;
    amount: {
        value: number,
        unit: string,
    };
}

export interface Hops {
    name: string;
    amount: {
        value: number,
        unit: string,
    };
} 

export interface Ingredients {
    malt: Array<Malt>;
    hops: Array<Hops>;
}

export interface Beer {
    id: number;
    name: string;
    first_brewed: string;
    ingredients: Ingredients;
    food_pairing: Array<string>;
}

export interface BeersParams {
    page?: number;
    per_page?: number;
}

export interface Beers {
    toggle_dialog: boolean;
    beer: Beer,
    is_loading: boolean;
    beers: Array<Beer>;
    params: BeersParams
}
