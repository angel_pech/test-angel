import { Module } from 'vuex';

import { Root, Beers } from '@/store/types';
import BeersService from '@/services/Beers';

const beers: Module<Beers, Root> = {
	namespaced: true,

	state: {
		is_loading: false,
		beers: [],
		params: {
			page: 1,
			per_page: 30,
		},
		toggle_dialog: false,
		beer: {
			id: 0,
			name: '',
			first_brewed: '',
			ingredients: {
				malt: [],
				hops: [],
			},
			food_pairing: [],
		},
	},

	mutations: {
		LOAD_BEERS(state, payload) {
			state.beers = payload;
		},

		SET_PARAMS(state, payload) {
			state.params = payload;
		},

		SET_LOADING(state, payload) {
			state.is_loading = payload;
		},

		OPEN_DIALOG(state, payload) {
			state.toggle_dialog = true;
			state.beer = payload;
		},

		CLOSE_DIALOG(state) {
			state.toggle_dialog = false;
			state.beer = {
				id: 0,
				name: '',
				first_brewed: '',
				ingredients: {
					malt: [],
					hops: [],
				},
				food_pairing: [],
			};
		},
	},

	actions: {
		async SEARCH({ commit, state }, payload): Promise<void> {
			commit('SET_PARAMS', payload);

			commit('SET_LOADING', true);

			const DATA = await BeersService.getAll(state.params);

			commit('LOAD_BEERS', DATA);

			commit('SET_LOADING', false);
		},

		OPEN_DIALOG({ commit }, payload): void {
			commit('OPEN_DIALOG', payload);
		},

		CLOSE_DIALOG({ commit }): void {
			commit('CLOSE_DIALOG');
		},
	},

	modules: {},
};

export default beers;
