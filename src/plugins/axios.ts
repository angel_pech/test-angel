import Vue from 'vue';
import Axios from 'axios';

// Configuración: https://axios-http.com/es/docs/req_config
const configuracion = {
	baseURL: process.env.VUE_APP_API_BASE_URL || 'localhost',
	/* headers: {
		'Access-Control-Allow-Origin': '*',
		'Access-Control-Allow-Credentials': true,
	},
	withCredentials: true,
	crossDomain: true, */
	dataType: 'json',
};

export const axios = Axios.create(configuracion);

axios.interceptors.response.use(undefined, (error) => {
	if (error.response.status === 401 || error.response.data.message === '401 Unauthorized') {
		localStorage.removeItem('sesion');
		window.location.reload();
	}
});

function pluginFuncion(vue: typeof Vue): void {
	const vue_temporal = vue;
	vue_temporal.prototype.$axios = axios;
}

const plugin_axios = {
	install: pluginFuncion,
};

Vue.use(plugin_axios);

export default plugin_axios;
