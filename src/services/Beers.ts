import { axios } from '@/plugins/axios';
import { BeersParams, Beers as BeersInterface } from '@/store/types';

export default class Beers {
	static async getAll(params: BeersParams): Promise<BeersInterface> {
		const RESPONSE = await axios.get('/beers', { params });

		return RESPONSE.data;
	}
}
